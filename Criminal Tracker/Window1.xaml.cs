﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.IO;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Configuration;



namespace Criminal_Tracker
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    /// 
    public partial class Window1 : Window
    {
        Add_Criminal addCriminal;
        DataTable dt;
        SqlDataAdapter adp;
        public int a;
        

        public Window1()
        {
            InitializeComponent();
            


        }
        string ConnStr = ConfigurationManager.ConnectionStrings["Criminal_Tracker.Properties.Settings.Database1ConnectionString"].ConnectionString;


        private void add_Click(object sender, RoutedEventArgs e)
        {


            addCriminal = new Add_Criminal();
            
            addCriminal.Show();
            
            
        }
       

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            string query = null;
            using (SqlConnection conn = new SqlConnection(ConnStr))
            {
                conn.Open();
                query = "Select * from info";
                //if (comboBox.SelectedIndex == 0)
                //{
                //    query = "Select * from info where Id=" + Convert.ToInt32(Search.Text);
                //}
                //else if (comboBox.SelectedIndex == 1)
                //{
                //    query = "Select * from info where [First Name] like '" + Search.Text + "%'";
                //}
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                    {
                        DataTable dt = new DataTable("info");

                        adp.Fill(dt);
                        dataGrid.ItemsSource = dt.DefaultView;



                       
                    }

                    conn.Close();
                }
            }
            
            
            
            
        }

       

        private void search_Click(object sender, RoutedEventArgs e)
        {
           
        }
      

        private void settings_Click(object sender, RoutedEventArgs e)
        {
            Password win = new Password();
            win.Show();
        }
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Application.Current.Shutdown();
        }
        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void Cut_Click(object sender, RoutedEventArgs e)
        {
            Search.Cut();
        }
        private void Copy_Click(object sender, RoutedEventArgs e)
        {
            Search.Copy();
        }
        private void Paste_Click(object sender, RoutedEventArgs e)
        {
            Search.Paste();
        }
        private void Undo_Click(object sender, RoutedEventArgs e)
        {
            Search.Undo();
        }

        private void Search_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(Search.Text=="")
            {
                string query = null;
                using (SqlConnection conn = new SqlConnection(ConnStr))
                {
                    conn.Open();
                    query = "Select * from info";
                    //if (comboBox.SelectedIndex == 0)
                    //{
                    //    query = "Select * from info where Id=" + Convert.ToInt32(Search.Text);
                    //}
                    //else if (comboBox.SelectedIndex == 1)
                    //{
                    //    query = "Select * from info where [First Name] like '" + Search.Text + "%'";
                    //}
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                        {
                            DataTable dt = new DataTable("info");

                            adp.Fill(dt);
                            dataGrid.ItemsSource = dt.DefaultView;




                        }

                        conn.Close();
                    }
                }
            }
            //if(comboBox.SelectedIndex==0)
            //{
            //    if (!char.IsDigit(e.Text, e.Text.Length - 1))
            //        e.Handled = true;
            //}
        }
       

        private void search_Click_1(object sender, RoutedEventArgs e)
        {
            string query = null;
            
            using (SqlConnection conn = new SqlConnection(ConnStr))
            {
                conn.Open();
                if (comboBox.SelectedIndex == 0 && !string.IsNullOrWhiteSpace(Search.Text))
                {
                    
                    query = "Select * from info where Id=" + Convert.ToInt32(Search.Text);
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                        {
                            dt = new DataTable("info");

                            adp.Fill(dt);
                            dataGrid.ItemsSource = dt.DefaultView;




                        }

                        conn.Close();
                    }
                }
                else if (comboBox.SelectedIndex == 1)
                {
                    query = "Select * from info where [First Name] like '" + Search.Text+"%'";
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                        {
                            dt = new DataTable("info");

                            adp.Fill(dt);
                            dataGrid.ItemsSource = dt.DefaultView;




                        }

                        conn.Close();
                    }
                }
                
                
                
            }
        }

        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }
        private void DG_Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            Hyperlink link = (Hyperlink)e.OriginalSource;
            Process.Start(link.NavigateUri.AbsoluteUri);
        }

        private void refresh_Click(object sender, RoutedEventArgs e)
        {
            string query = null;
            using (SqlConnection conn = new SqlConnection(ConnStr))
            {
                conn.Open();
                query = "Select * from info";
                //if (comboBox.SelectedIndex == 0)
                //{
                //    query = "Select * from info where Id=" + Convert.ToInt32(Search.Text);
                //}
                //else if (comboBox.SelectedIndex == 1)
                //{
                //    query = "Select * from info where [First Name] like '" + Search.Text + "%'";
                //}
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                    {
                        DataTable dt = new DataTable("info");

                        adp.Fill(dt);
                        dataGrid.ItemsSource = dt.DefaultView;




                    }

                    conn.Close();
                }
            }
        }

        private void edit_Click(object sender, RoutedEventArgs e)
        {
            if (dataGrid.SelectedIndex >= 0)
            {
                DataRowView drv = (DataRowView)dataGrid.SelectedItem;
                int result = Convert.ToInt32(drv["Id"]);
                string fName = (drv["First Name"]).ToString();
                string lName = (drv["Last Name"]).ToString();
                string address = (drv["Address"]).ToString();
                string nationality = (drv["Nationality"]).ToString();
                string fatherName = (drv["Father Name"]).ToString();
                string motherName = (drv["Mother Name"]).ToString();
                string felony = (drv["Felony"]).ToString();
                string caught = (drv["Caught"]).ToString();
                string punishment = (drv["Punishment"]).ToString();
                string trialOver = (drv["Trial Over"]).ToString();
                string image = (drv["image"]).ToString();
                Edit edit = new Edit(result, fName, lName, address, nationality, fatherName, motherName, felony, caught, punishment, trialOver, image);
                edit.Show();
            }
            else
            {
                MessageBox.Show("Please select a row.");
            }
        }

        private void Search_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (comboBox.SelectedIndex == 0)
            {
                if (!char.IsDigit(e.Text, e.Text.Length - 1))
                    e.Handled = true;
            }
        }

        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Search.Text = "";
        }
    }
}

