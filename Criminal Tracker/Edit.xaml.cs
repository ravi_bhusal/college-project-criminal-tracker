﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
namespace Criminal_Tracker
{
    /// <summary>
    /// Interaction logic for Edit.xaml
    /// </summary>
    public partial class Edit : Window
    {
        Uri uri;
        FileDialog fldlg;
        string filePath;
        string imageName = null;
        string ConnStr = ConfigurationManager.ConnectionStrings["Criminal_Tracker.Properties.Settings.Database1ConnectionString"].ConnectionString;
        public Edit(int result,string FName,string LName, string Address,string Nationality, string FatherName, string MotherName, string Felony, string Caught, string Punishment, string TrialOver, string Image)
        {
            InitializeComponent();
            id.Content = result.ToString();
            fName.Text = FName;
            lName.Text = LName;
            address.Text = Address;
            nationality.Text = Nationality;
            fatherName.Text = FatherName;
            motherName.Text = MotherName;
            felony.Text = Felony;
            punishment.Text = Punishment;
            yesCaught.IsChecked = Convert.ToBoolean(Caught);
            yesTrialOver.IsChecked = Convert.ToBoolean(TrialOver);
            if (!string.IsNullOrWhiteSpace(Image))
            {
                uri = new Uri(Image);
            }
            else
            {
                uri = new Uri("pack://application:,,,/Resources/images.bmp");
            }
            var bitmap = new BitmapImage(uri);
            image.Source = bitmap;
        }

        private void addImage_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string folderpath = Directory.GetParent(Environment.CurrentDirectory).Parent.FullName + "\\Images\\";
                fldlg = new OpenFileDialog();
                fldlg.InitialDirectory = Environment.SpecialFolder.MyPictures.ToString();
                fldlg.Filter = "Image File (*.jpg;*.bmp;*.gif;*.png;)|*.jpg;*.bmp;*.gif;*.png;";
                fldlg.ShowDialog();
                {
                    // strName = fldlg.SafeFileName;
                    imageName = fldlg.FileName;

                    ImageSourceConverter isc = new ImageSourceConverter();
                    image.SetValue(System.Windows.Controls.Image.SourceProperty, isc.ConvertFromString(imageName));
                    if (!Directory.Exists(folderpath))
                    {
                        Directory.CreateDirectory(folderpath);
                    }
                    filePath = folderpath + System.IO.Path.GetFileName(imageName);
                    System.IO.File.Copy(imageName, filePath, true);

                }
                fldlg = null;
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private void reset_Click(object sender, RoutedEventArgs e)
        {
            var uri = new Uri("pack://application:,,,/Resources/images.bmp");
            var bitmap = new BitmapImage(uri);
            image.Source = bitmap;
            imageName = null;
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            string query = "update info set [First Name]='" + fName.Text + "',[Last Name]='" + lName.Text + "',address='" + address.Text +
                         "',nationality='" + nationality.Text + "',[Father Name]='" + fatherName.Text + "',[Mother Name]='" + motherName.Text +
                         "',felony='" + felony.Text + "',caught=" + Convert.ToInt32(yesCaught.IsChecked) + ",punishment='" + punishment.Text +
                         "',[Trial Over]=" + Convert.ToInt32(yesTrialOver.IsChecked) +",image='"+filePath+"' where Id=" + Convert.ToInt32(id.Content);
            using (SqlConnection conn = new SqlConnection(ConnStr))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                   int result= cmd.ExecuteNonQuery();
                    if(result==1)
                    {
                        MessageBox.Show("Criminal Data Updated");
                        this.Close();
                    }
                }
            }
        }
    }
}
