﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Configuration;
namespace Criminal_Tracker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //string connStr = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=D:\\projects\\Windows\\Criminal Tracker\\Criminal Tracker\\Database1.mdf;Integrated Security=True";
        string ConnStr = ConfigurationManager.ConnectionStrings["Criminal_Tracker.Properties.Settings.Database1ConnectionString"].ConnectionString;
        public MainWindow()
        {
            InitializeComponent();
        }
        string userName = null, _password = null;


       

        private void password_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            

        }

      

        private void login_Click_1(object sender, RoutedEventArgs e)
        {
            SqlConnection conn = new SqlConnection(ConnStr);

            conn.Open();
            string query = "Select username, password from password where Id=1";
            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                userName = reader["username"].ToString();
                _password = reader["password"].ToString();
            }



            conn.Close();


            if (username.Text.Equals(userName) && password.Password.Equals(_password))
            {
                Window1 win = new Window1();
                win.Show();
                this.Hide();

            }
            else
            {
                MessageBox.Show("Username or Password is incorrect.");
            }

        }

        private void cancel_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
