﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;

namespace Criminal_Tracker
{
    /// <summary>
    /// Interaction logic for Password.xaml
    /// </summary>
    public partial class Password : Window
    {
        string connStr = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=D:\\projects\\Windows\\Criminal Tracker\\Criminal Tracker\\Database1.mdf;Integrated Security=True";
        public Password()
        {
            InitializeComponent();
        }

        private void changeButton_Click(object sender, RoutedEventArgs e)
        {
            if (passwordBox.Password == confirmPasswordBox.Password)
            {
                using (SqlConnection conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    string query = "Update password set username='" + newUserName.Text + "',password='" + passwordBox.Password + "' where id=1";
                        
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                       
                        int result = cmd.ExecuteNonQuery();
                        if (result == 1)
                        {
                            MessageBox.Show("Username and Password Changed.");
                            this.Close();



                        }
                        conn.Close();

                    }
                }
            }
            else
            {
                MessageBox.Show("Password couldn't be confirmed.");
            }
        }

        private void reset_Click(object sender, RoutedEventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                conn.Open();

                string query = "Update password set username='admin', password='admin' where id=1";
                    
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {

                    int result = cmd.ExecuteNonQuery();
                    if (result == 1)
                    {
                        MessageBox.Show("Username and Password Reset.");
                        this.Close();



                    }
                    conn.Close();

                }
            }
        }
    }
}
