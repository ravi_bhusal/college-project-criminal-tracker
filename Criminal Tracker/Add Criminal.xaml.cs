﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.IO;
using Microsoft.Win32;
using System.Drawing.Imaging;
using System.Data;
using System.Drawing;
using System.Configuration;
namespace Criminal_Tracker
{
    /// <summary>
    /// Interaction logic for Add_Criminal.xaml
    /// </summary>
    public partial class Add_Criminal : Window
    {
      
        int caughtVar;
        int trialVar;
        public Add_Criminal()
        {
            InitializeComponent();

        }
        string filePath;
        FileDialog fldlg;
        
        
        string imageName;//= "D:\\projects\\Windows\\Criminal Tracker\\Criminal Tracker\\Resources\\images.bmp";
                         // String imageName = "pack://application:,,,/Resources/images.bmp";


        // public string connStr = "Data Source = (LocalDB)\\MSSQLLocalDB; AttachDbFilename = |DataDirectory|Database1.mdf; Integrated Security = True";
       // public string connStr = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=D:\\projects\\Windows\\Criminal Tracker\\Criminal Tracker\\Database1.mdf;Integrated Security=True";
        //  public string connStr = "Data source=(LocalDb)\\MSSQLLocalDB; Initial Catalog=Database1.mdf;integrated security=True;MultipleActiveResultSets=True";
        string ConnStr = ConfigurationManager.ConnectionStrings["Criminal_Tracker.Properties.Settings.Database1ConnectionString"].ConnectionString;


        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            caughtVar = Convert.ToInt32(yesCaught.IsChecked);
            trialVar = Convert.ToInt32(yesTrialOver.IsChecked);


            if (!string.IsNullOrWhiteSpace(imageName))
            {

                //FileStream fs = new FileStream(imageName, FileMode.Open,
                //                FileAccess.Read);

                //data = new byte[fs.Length];
                //fs.Read(data, 0, System.Convert.ToInt32(fs.Length));
              
                //fs.Close();
               
            }
            else
            {
                imageName = "";
            }

            using (SqlConnection conn = new SqlConnection(ConnStr))
            {
                conn.Open();

                string query = "insert into info " +
                    "values(" + Convert.ToInt32(id.Content) + ",'" + fName.Text + "','" + lName.Text + "','" + address.Text + "','" + nationality.Text +
                              "','" + fatherName.Text + "','" + motherName.Text + "','" + felony.Text + "'," + caughtVar + ",'" + punishment.Text + "'," + trialVar + ",'"+filePath+"')";
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                   // MemoryStream stream = new MemoryStream();


                  //  cmd.Parameters.AddWithValue("@image", data);
                    int result = cmd.ExecuteNonQuery();
                    if (result == 1)
                    {
                        MessageBox.Show("Criminal Data inserted.");
                        this.Close();

                        

                       

                    }
                    conn.Close();

                }
            }
            


        }

        private void reset_Click(object sender, RoutedEventArgs e)
        {
            var uri = new Uri("pack://application:,,,/Resources/images.bmp");
            var bitmap = new BitmapImage(uri);
            image.Source = bitmap;
            imageName = null;
        }

        private void addImage_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string folderpath = System.IO.Directory.GetParent(Environment.CurrentDirectory).Parent.FullName + "\\Images\\";
                fldlg = new OpenFileDialog();
                fldlg.InitialDirectory = Environment.SpecialFolder.MyPictures.ToString();
                fldlg.Filter = "Image File (*.jpg;*.bmp;*.gif;*.png;)|*.jpg;*.bmp;*.gif;*.png;";
                fldlg.ShowDialog();
                {
                    // strName = fldlg.SafeFileName;
                    imageName = fldlg.FileName;

                    ImageSourceConverter isc = new ImageSourceConverter();
                    image.SetValue(System.Windows.Controls.Image.SourceProperty, isc.ConvertFromString(imageName));
                    if (!Directory.Exists(folderpath))
                    {
                        Directory.CreateDirectory(folderpath);
                    }
                     filePath = folderpath + System.IO.Path.GetFileName(imageName);
                    System.IO.File.Copy(imageName, filePath, true);

                }
                fldlg = null;
            }
            catch (Exception ex)
            {
                return;
            }
        }
        

        private void AddWindow_Loaded(object sender, RoutedEventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(ConnStr))
            {
                conn.Open();
                String sql = "Select Id from info where Id=(Select MAX(Id) from info)";
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    cmd.ExecuteNonQuery();
                    using (SqlDataReader oReader = cmd.ExecuteReader())
                    {
                        int? _id = null;
                        while (oReader.Read())
                        {
                            _id = oReader.GetInt32(0) + 1;
                            // string id = oReader["Id"].ToString();                                                     
                            //   _id = Convert.ToInt32(id) + 1;

                            // addCriminal.id.Content = _id.ToString();


                        }
                        id.Content = _id.ToString();
                        if (string.IsNullOrWhiteSpace(id.Content.ToString()))
                        {
                            id.Content = "1";
                        }

                        else
                        {
                            id.Content = _id.ToString();
                        }
                        conn.Close();
                    }

                }
            }
        }
       
    }
}

